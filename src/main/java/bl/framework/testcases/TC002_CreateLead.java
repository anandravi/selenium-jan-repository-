package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC002_CreateLead extends SeleniumBase {
Select sel;
	
	@Test
	public  void ceateLead() throws InterruptedException {
		
		startApp("chrome","http://leaftaps.com/opentaps");
		
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		
		WebElement eleCRMSFALink = locateElement("xpath", "//a[contains(text(), 'CRM/SFA')]");
		click(eleCRMSFALink);

		WebElement eleCreateLeadLink = locateElement("xpath", "//a[text()= 'Create Lead']");
		click(eleCreateLeadLink);
		
		WebElement eleCompanyNameTxt  = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCompanyNameTxt, "ASP Labs");
		
		WebElement eleFirstNameTxt  = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleFirstNameTxt, "Anand");
		
		WebElement eleLastNameTxt  = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleLastNameTxt, "R");
		
		WebElement eleSourceDD = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingValue(eleSourceDD, "LEAD_PARTNER");
		
		WebElement eleMarketingDD = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(eleMarketingDD, "Catalog Generating Marketing Campaigns");
		
		WebElement eleFirstNameLocalTxt = locateElement("name", "firstNameLocal");
		clearAndType(eleFirstNameLocalTxt, "Matt");
		
		WebElement eleLastNameLocalTxt = locateElement("name","lastNameLocal");
		clearAndType(eleLastNameLocalTxt, "Booker");

		WebElement eleSalutationTxt = locateElement("id", "createLeadForm_personalTitle");
		clearAndType(eleSalutationTxt, "TestSalutation");
		
		WebElement eleTitleTxt = locateElement("id", "createLeadForm_generalProfTitle");
		clearAndType(eleTitleTxt, "TestTitle");
		
		WebElement eleDOB = locateElement("name", "birthDate");
		clearAndType(eleDOB, "2/5/91");

		WebElement eleAnnualRevenuTxt = locateElement("name", "annualRevenue");
		clearAndType(eleAnnualRevenuTxt, "2000000");
		
		WebElement eleDeptTxt = locateElement("id", "createLeadForm_departmentName");
		clearAndType(eleDeptTxt, "Business Analyst");

		WebElement elePreferredCountryDD = locateElement("id", "createLeadForm_currencyUomId");
		selectDropDownUsingText(elePreferredCountryDD, "GBP - British Pound");

		WebElement eleIndustryDD = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingValue(eleIndustryDD, "IND_INSURANCE");
		
		WebElement eleNumOfEmployessTxt = locateElement("id", "createLeadForm_numberEmployees");
		clearAndType(eleNumOfEmployessTxt, "50");

		WebElement eleSICCode = locateElement("name", "sicCode");
		clearAndType(eleSICCode, "121");

		WebElement eleOwnerShipDD = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingText(eleOwnerShipDD, "LLC/LLP");
		
		WebElement eleTickerSymbolTxt = locateElement("name", "tickerSymbol");
		clearAndType(eleTickerSymbolTxt, "121");
		
		
		WebElement eleCountryCodeTxt = locateElement("xpath", "//input[@id='createLeadForm_primaryPhoneCountryCode']");
		clearAndType(eleCountryCodeTxt, "91");
		Thread.sleep(1000);

		WebElement elePhonenumbTxt = locateElement("xpath", "//input[@id='createLeadForm_primaryPhoneNumber']");
		clearAndType(elePhonenumbTxt, "9999999988");
		
		WebElement eleEmailTxt = locateElement("id", "createLeadForm_primaryEmail");
		clearAndType(eleEmailTxt, "Test121@gmail.com");
		
		WebElement eleToNameTxt = locateElement("id", "createLeadForm_generalToName");
		clearAndType(eleToNameTxt, "Matt");
		
		WebElement eleAddress1Txt = locateElement("name", "generalAddress1");
		clearAndType(eleAddress1Txt, "No.12, Flora Homes");
		
		WebElement elePostalCodeTxt = locateElement("id", "createLeadForm_generalPostalCode");
		clearAndType(elePostalCodeTxt, "600088");
		
		WebElement eleCityTxt = locateElement("id", "createLeadForm_generalCity");
		clearAndType(eleCityTxt, "Kanchipuram");
		
		WebElement eleCountryDD = locateElement("id", "createLeadForm_generalCountryGeoId");
		selectDropDownUsingText(eleCountryDD,"India");
		Thread.sleep(2000);

		WebElement eleStateDD = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		selectDropDownUsingText(eleStateDD,"TAMILNADU");
		
		WebElement eleSubmitBtn = locateElement("name", "submitButton");
		click(eleSubmitBtn);

		
		
		
	}
	
	

}
